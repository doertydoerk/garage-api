# Garage API


## Gerneral Information

By default, the API will listen on port 8080. You may change the port launching the API like so:
```shell
$ ./garage-api --port=1234
```
This will launch the API on port 1234.

A list of available endpoint on the index page.

The APi is based on [this](https://www.einplatinencomputer.com/raspberry-pi-ultraschallsensor-hc-sr04-ansteuern-entfernung-messen/) tutorial and is using [go-rpio](https://github.com/stianeikeland/go-rpio).

The GPIO layout look like this:
![Raspberry 3 Mobell B GPIO Layout](./Raspberry-Pi-GPIO-Layout-Model-B-Plus-rotated-2700x900-1024x341.png)
You may refer to [here](https://pkg.go.dev/github.com/stianeikeland/go-rpio@v4.2.0+incompatible?tab=doc) for connector layout.


## Door Switch
The door is toggled by switching pin #18 on the RaspPI for 250ms.

## Ultrasonic Sensor Tutorial (german)


