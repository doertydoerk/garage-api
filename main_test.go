package main

import (
	"testing"

	_ "github.com/stretchr/testify"
	"github.com/stretchr/testify/assert"
)

func TestPort(t *testing.T) {
	tests := []struct {
		name string
		input []string
		expected string
	}{
		{"success",[]string{"--port=1234"},":1234"},
		{"failure", []string{"--foo=1234"},":8080"},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			assert.Equal(t, test.expected, getPort(test.input))
		})
	}
}

/*
func TestPerformance(t *testing.T) {
	for i := 0; i < 15; i++ {
		resp1, err1 := http.Get("http://lab_pi:8080/toggle")
		resp2, err2 := http.Get("http://lab_pi:8080/status")
		assert.Nil(t, err1)
		assert.Equal(t, 200, resp1.StatusCode)
		
		assert.Nil(t, err2)
		assert.Equal(t, 200, resp2.StatusCode)
	}
}
*/