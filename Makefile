swagger:
	swagger generate spec -o ./swagger.yaml --scan-models
	python swagger-yaml-to-html.py < ./swagger.yaml > static/index.html

serve_local:
	CONTEXT=dev go run main.go

serve_staging:
	CONTEXT=staging go run main.go

serve_prod:
	CONTEXT=prod go run main.go

deploy:
	GOOS=linux GOARCH=arm GOARM=7 go build -o ./build/garage-api
	scp ./build/garage-api pi@garage:/home/pi/Downloads
	ssh -t pi@garage "sudo mv /home/pi/Downloads/garage-api /opt && sudo chown -R root:root /opt/garage-api && sudo systemctl restart garageApi.service"