package src

import (
	"encoding/json"
	"fmt"
	"net/http"
)

// Controller that receive handles request made to the APU
type Controller struct {
	doorManager DoorManager
}

// Init sets up the controller
func (c *Controller) Init() {
	c.doorManager = DoorManager{}
	c.doorManager.Init()
}

// Toggle lets you open and close the garage door
func (c *Controller) Toggle(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		ErrorLogger.Println("HTTP method not supported")
		c.errorHandler(w, r, http.StatusMethodNotAllowed)
	}

	err := c.doorManager.Toggle(r.Header.Get("device_name"))
	if err != nil {
		ErrorLogger.Println(err.Error())
		c.errorHandler(w, r, http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "text/plain")
	_, err = fmt.Fprintf(w, "ok")
	if err != nil {
		ErrorLogger.Println(err.Error())
		c.errorHandler(w, r, http.StatusInternalServerError)
	}
}

// Status return the status of the door (open or closed)
func (c *Controller) Status(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		ErrorLogger.Println("HTTP method not supported")
		c.errorHandler(w, r, http.StatusMethodNotAllowed)
	}

	b, err := json.Marshal(c.doorManager.Status())
	if err != nil {
		ErrorLogger.Println(err.Error())
		c.errorHandler(w, r, http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/json")
	if _, err := w.Write(b); err != nil {
		ErrorLogger.Println(err.Error())
		c.errorHandler(w, r, http.StatusInternalServerError)
	}
}

// Ping returns OK if the system is up and running
func (c *Controller) Ping(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		ErrorLogger.Println("HTTP method not supported")
		c.errorHandler(w, r, http.StatusMethodNotAllowed)
	}

	w.Header().Set("Content-Type", "text/plain")
	_, err := fmt.Fprintf(w, "ok")
	if err != nil {
		ErrorLogger.Println(err.Error())
		c.errorHandler(w, r, http.StatusInternalServerError)
	}
}

func (c *Controller) errorHandler(w http.ResponseWriter, r *http.Request, status int) {
	w.WriteHeader(status)
	if status == http.StatusNotFound {
		ErrorLogger.Println("404 - not found: " + r.Host + r.RequestURI)

		if _, err := fmt.Fprint(w, "custom 404"); err != nil {
			ErrorLogger.Println(err.Error())
		}
	}
}
