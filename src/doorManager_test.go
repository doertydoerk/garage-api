package src

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestCalcDistance(t *testing.T) {
	start := time.Now()
	end := start.Add((1 * time.Second))

	doorManager := DoorManager{
		distanceThreshold: 20,
	}

	dist := doorManager.calculateDistance(start, end)
	assert.Equal(t, 17150, dist)
}
