package src

import (
	"errors"
	"fmt"
	"os"
	"time"

	"github.com/stianeikeland/go-rpio/v4"
)

const (
	open         = "open"
	closed       = "closed"
	unknown      = "status unknown"
	sonicSpeed   = 34300 // cm/sec
	zeroDistance = 0
	relay        = 18
	trigger      = 20
	echo         = 21
)

type (
	// DoorManager has all functionality around the garage door
	DoorManager struct {
		distanceThreshold int
		rpioIsLocked      bool
	}

	// StatusResponse will be return as the result of a status request
	StatusResponse struct {
		Status    string
		Distance  int
		Threshold int
	}
)

// Init allows to setup the DoorManager
func (m *DoorManager) Init() {
	if isContextProduction() {
		m.distanceThreshold = 280
	} else {
		m.distanceThreshold = 20
	}

	// // NOTE: make sure doorSwitch is high(!) to begin with as relay will switch on low!
	if err := m.setDoorSwitchHigh(); err != nil {
		// TODO logging
	}
}

// Toggle actually toggles the door by switching the relay to low for 1/4 second.Toggle
// NOTE: the relay closes on low power Not on high!
func (m *DoorManager) Toggle(device string) error {
	if m.rpioIsLocked {
		return errors.New("rpio is locked")
	}

	m.rpioIsLocked = true
	go m.toggleDoor()

	if isContextProduction() {
		InfoLogger.Printf("Door was toggled: %s\n", device)
	}

	return nil
}

func (m *DoorManager) toggleDoor() {
	err := rpio.Open()
	if err != nil {
		m.rpioIsLocked = false
		return
	}

	doorSwitch := rpio.Pin(relay)
	doorSwitch.Output()

	doorSwitch.Low()
	time.Sleep(time.Second / 8)
	doorSwitch.High()

	err = rpio.Close()
	if err == nil {
		m.rpioIsLocked = false
	}
}

// Status return the status of the door (open or closed)
func (m *DoorManager) Status() StatusResponse {
	if m.rpioIsLocked {
		return m.createStatusResponse(unknown, zeroDistance)
	}

	m.rpioIsLocked = true
	distance, err := m.getDistance()
	if err != nil || distance == zeroDistance {
		return m.createStatusResponse(unknown, zeroDistance)
	}

	switch distance < m.distanceThreshold {
	case true:
		return m.createStatusResponse(open, distance)
	case false:
		return m.createStatusResponse(closed, distance)
	default:
		return m.createStatusResponse(unknown, distance)
	}
}

func (m *DoorManager) getDistance() (int, error) {
	err := rpio.Open()
	if err != nil {
		//		log.Error("server maybe not RaspPi: " + err.Error())
		m.rpioIsLocked = false
		return zeroDistance, err
	}

	trigger := rpio.Pin(trigger)
	trigger.Output()
	echo := rpio.Pin(echo)
	echo.Input()

	var start time.Time
	var end time.Time

	t := (1 * time.Millisecond) / 100

	trigger.High()
	time.Sleep(t)
	trigger.Low()

	for echo.Read() == rpio.Low {
		start = time.Now()
	}

	for echo.Read() == rpio.High {
		end = time.Now()
	}

	err = rpio.Close()
	if err == nil {
		distance := m.calculateDistance(start, end)
		m.rpioIsLocked = false
		return distance, nil
	}

	return 0, err
}

// calculateDistance returns the distance in cm
func (m *DoorManager) calculateDistance(start time.Time, end time.Time) int {
	// note: duration is in ns
	duration := (end.Sub(start))
	distance := int(duration.Seconds()*sonicSpeed) / 2
	return distance
}

func (m *DoorManager) createStatusResponse(status string, distance int) StatusResponse {
	return StatusResponse{
		Status:    status,
		Distance:  distance,
		Threshold: m.distanceThreshold,
	}
}

func (m *DoorManager) setDoorSwitchHigh() error {
	m.rpioIsLocked = true
	err := rpio.Open()
	if err != nil {
		//		log.Error("server may not be RaspPi: " + err.Error())
		m.rpioIsLocked = false
		return err
	}

	doorSwitch := rpio.Pin(relay)
	doorSwitch.Output()
	doorSwitch.High()

	err = rpio.Close()
	if err == nil {
		m.rpioIsLocked = false
		return nil
	}

	return fmt.Errorf("error closing RPIO: %s", err)
}

func isContextProduction() bool {
	return os.Getenv("CONTEXT") == "production"
}
