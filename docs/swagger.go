package docs

// https://medium.com/@pedram.esmaeeli/generate-swagger-specification-from-go-source-code-648615f7b9d9

import (
	"gitlab.com/doertydoerk/garage-api/src"
)

// swagger:route GET /toggle main-tag
// Toggle lets you open and close the garage door
// responses:
//   200: DoorManager
// This text will appear as description of your response body.

// swagger:response DoorManager
type DoorManagerResponseWrapper struct {
	// in:body
	Body src.DoorManager
}
