package main

import (
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	// This line is necessary for go-swagger to find the docs!
	_ "github.com/pdrum/swagger-automation/docs"
	"gitlab.com/doertydoerk/garage-api/src"
)

func getPort(args []string) string {
	for _, arg := range args {
		if strings.HasPrefix(arg, "--port=") {
			newPort := strings.TrimPrefix(arg, "--port=")
			return ":" + newPort
		}
	}
	return ":8080"
}

func getStaticDirectory() string {
	if os.Getenv("CONTEXT") == "dev" {
		return "./static"
	}
	ex, _ := os.Executable()
	return filepath.Dir(ex) + "/static"
}

func launchAPI() {
	controller := src.Controller{}
	controller.Init()

	http.DefaultServeMux.Handle("/", http.FileServer(http.Dir(getStaticDirectory())))
	http.DefaultServeMux.HandleFunc("/toggle", controller.Toggle)
	http.DefaultServeMux.HandleFunc("/status", controller.Status)
	http.DefaultServeMux.HandleFunc("/ping", controller.Ping)

	src.InfoLogger.Printf("CONTEXT: %s\n", os.Getenv("CONTEXT"))
	src.InfoLogger.Printf("Garage API is listening on port %s\n", getPort(os.Args))
	log.Fatal(http.ListenAndServe(getPort(os.Args), nil))
}

func main() {
	launchAPI()
}
